FROM python:3.9
RUN pip install flask requests
ARG CONTAINER
ENV CONTAINER=$CONTAINER
WORKDIR /app
COPY . /app
EXPOSE 5000
CMD ["printenv", "CONTAINER"]